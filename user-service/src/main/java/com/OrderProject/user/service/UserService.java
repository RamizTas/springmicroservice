package com.OrderProject.user.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.OrderProject.user.VO.Product;
import com.OrderProject.user.VO.ResponseTemplateVO;
import com.OrderProject.user.entity.User;
import com.OrderProject.user.repository.UserRepository;

@Service
public class UserService {
	
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RestTemplate restTemplate;
	
	public User saveUser(User user) {
		return userRepository.save(user);
		
	}

	public User findUserById(Long id) {
		return userRepository.findByUserId(id);
	}

	public ResponseTemplateVO getUserWithProduct(Long userId) {
		ResponseTemplateVO vo = new ResponseTemplateVO();
	   User user = userRepository.findByUserId(userId);
	
	   Product product = 
		   restTemplate.getForObject("http://PRODUCT-SERVICE/products/" + user.getProductId()
		                             ,Product.class);
	      vo.setUser(user);
	      vo.setProduct(product);
	      
	      return vo;

	
		
	}
	
	
	

}
