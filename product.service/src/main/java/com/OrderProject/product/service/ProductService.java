package com.OrderProject.product.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.OrderProject.product.entity.Product;
import com.OrderProject.product.repository.ProductRepository;


@Service
public class ProductService  {
	
	@Autowired
	private ProductRepository productRepository;

	public Product saveProduct(Product product) {
		return productRepository.save(product);
	}

	public Product findProductById(Long productId) {
		 return productRepository.findByproductId(productId);
				 
	}
	
	//deneme
	
	
	
	

}
